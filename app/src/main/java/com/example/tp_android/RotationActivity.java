package com.example.tp_android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RotationActivity extends AppCompatActivity {
    private TextView nbRotations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotation);

        this.nbRotations = findViewById(R.id.textViewRotation);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d("MesLogs","Passage onSaveInstanceState");

        outState.putString("nbRotations", nbRotations.getText().toString());

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d("MesLogs","Passage onRestoreInstanceState");

        String nbRotations = savedInstanceState.getString("nbRotations");
        Integer nbr = Integer.valueOf(nbRotations);
        nbr++;
        this.nbRotations.setText(nbr.toString());
    }

    public void goBackToMainActivity(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
