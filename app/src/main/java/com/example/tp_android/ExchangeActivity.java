package com.example.tp_android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ExchangeActivity extends AppCompatActivity {
    private Button buttonExchange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);

        this.buttonExchange = findViewById(R.id.buttonExchange);
        buttonExchange.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                exchange(v);
            }
        });
    }

    public void exchange(View view) {
        TextView text1 = findViewById(R.id.textViewExchange1);
        TextView text2 = findViewById(R.id.textViewExchange2);

        String safeBox = text1.getText().toString();
        text1.setText(text2.getText().toString());
        text2.setText(safeBox);

        Log.d("MesLogs", "Fin échange");
    }

    public void goBackToMainActivity(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
