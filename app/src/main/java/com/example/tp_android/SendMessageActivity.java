package com.example.tp_android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class SendMessageActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
    }

    public void goBackToMainActivity(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void sendMessage(View view) {
        EditText editTextMessageToSend = findViewById(R.id.editTextMessageToSend);
        String message = editTextMessageToSend.getText().toString();
        Intent intent = new Intent(this, ShowMessageActivity.class);
        intent.putExtra("message", message);
        startActivity(intent);
    }
}
