package com.example.tp_android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tp_android.model.Personne;

public class SendParcelableActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle SavedInstanceState) {
        super.onCreate(SavedInstanceState);
        setContentView(R.layout.activity_send_parcelable);
    }

    public void sendToShowParcelableActivity(View view) {
        EditText editTextFirstName = findViewById(R.id.editTextFirstName);
        String firstName = editTextFirstName.getText().toString();
        EditText editTextLastName = findViewById(R.id.editTextTextLastName);
        String lastName = editTextLastName.getText().toString();
        Personne personne = new Personne(lastName, firstName);
        Intent intent = new Intent(this, ShowParcelableActivity.class);
        intent.putExtra("personne", personne);
        startActivity(intent);
    }

    public void goBackToMainActivity(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
