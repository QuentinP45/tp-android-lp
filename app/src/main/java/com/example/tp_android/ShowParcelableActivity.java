package com.example.tp_android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tp_android.model.Personne;

public class ShowParcelableActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle SavedInstanceState) {
        super.onCreate(SavedInstanceState);
        setContentView(R.layout.activity_show_parcelable);

        Intent intent = getIntent();
        Personne personne = intent.getParcelableExtra("personne");

        TextView textViewShowParcelable = findViewById(R.id.textViewShowParcelable);
        textViewShowParcelable.setText(personne.getPrenom() + ' ' + personne.getNom());
    }

    public void goBackToSendParcelableActivity(View view) {
        startActivity(new Intent(this, SendParcelableActivity.class));
    }

    public void goBackToMainActivity(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
