package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /////////////// LIFECYCLE CALLBACKS ///////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, "Passage onCreate", Toast.LENGTH_LONG).show();
        Log.d("Mes logs", "Passage onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Passage onStart", Toast.LENGTH_LONG).show();
        Log.d("Mes logs", "Passage on start");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Toast.makeText(this, "Passage onPause", Toast.LENGTH_LONG).show();
        Log.d("Mes logs", "Passage on pause");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Toast.makeText(this, "Passage onStop", Toast.LENGTH_LONG).show();
        Log.d("Mes logs", "Passage on stop");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Toast.makeText(this, "Passage on destroy", Toast.LENGTH_LONG).show();
        Log.d("Mes logs", "Passage on destroy");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Toast.makeText(this, "Passage onRestart", Toast.LENGTH_LONG).show();
        Log.d("Mes logs", "Passage on restart");
    }

    /////////////// ACTIVITIES ///////////////
    public void goToRotationActivity(View view) {
        startActivity(new Intent(this, RotationActivity.class));
    }

    public void goToExchangeActivity(View view) {
        startActivity(new Intent(this, ExchangeActivity.class));
    }

    public void goToIntentCallActivity(View view) {
        startActivity(new Intent(this, IntentCallActivity.class));
    }

    public void goToIntentPictureActivity(View view) {
        startActivity(new Intent(this, IntentPictureActivity.class));
    }

    public void goToSendMessageActivity(View view) {
        startActivity((new Intent(this, SendMessageActivity.class)));
    }

    public void goToSendParcelableActivity(View view) {
        startActivity((new Intent(this, SendParcelableActivity.class)));
    }
}