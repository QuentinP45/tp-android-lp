package com.example.tp_android;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class IntentCallActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_call);

        Button buttonCall = findViewById(R.id.buttonCall);
        buttonCall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                call(v);
            }
        });
    }

    private void composePhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void call(View view) {
        EditText phoneNumber = findViewById(R.id.editTextPhoneNumber);
        composePhoneNumber(phoneNumber.getText().toString());
    }

    public void goBackToMainActivity(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
